/**
 * Sell a product to a new owner
 * @param  {org.acme.online.shop.BuyProductList} buyProductList - buy products in list transaction
 * @transaction
 */

function buyProductList(ProductListReserved) {
  
  var getAccount = function(person) {
    var queryString = buildQuery("SELECT org.acme.online.shop.Account WHERE (owner == _$ownerParam) LIMIT 1");
    return query(queryString, { ownerParam: 'resource:'+person.getFullyQualifiedIdentifier() });    
  }
  var buyer = ProductListReserved.reserveList.reserver;
  var accountList = [];
  var reserveList = ProductListReserved.reserveList;
  var productList = ProductListReserved.reserveList.products;
  if (!buyer) {
    throw new Error('Product is not reserved by any person')
  }
  var promises = productList.map(function(product) {
    return getAccount(product.owner)
    		.then(function(account){
      			return { product: product, account: account[0] }
    		});
  });
  Promise.all(promises)
  	.then(function(productAccounts){
    	console.log(productAccounts);	
    	return productAccounts;    	
  	})
  	.then(function(productAccounts) {
    	return getAccount(buyer)
    			.then(function(result){
          			accountList.push(result[0]);
      				return result[0];
        		})
    			.then(function(account){
          			return {
                      productAccounts: productAccounts,
                      buyerAccount: account
                    }
        		})
    })
  	.then(function(basket){
    	console.log(basket);
    	basket.productAccounts.forEach(function(productAccount) {
          console.log('buyer balance' + basket.buyerAccount.balance);
          console.log('seller balance' + productAccount.account.balance);
          if (basket.buyerAccount.balance >= productAccount.product.price ) {
            basket.buyerAccount.balance -= productAccount.product.price;
            productAccount.account.balance += productAccount.product.price;
            productAccount.product.owner = basket.buyerAccount.owner;
            productAccount.product.state = 'OWNED';
	        accountList.push(productAccount.account);
          } else {
            throw new Error('Insufficient funds!');
          }
        });
    	console.log(accountList);
  	});
  
  	return getAssetRegistry('org.acme.online.shop.ReserveList')
        .then(function(reserveListRegistry) {
    		return reserveListRegistry.update(reserveList);
        })
  		.then(function() {
         	return getAssetRegistry('org.acme.online.shop.Product')
        })
        .then(function(productListRegistry) {
            return productListRegistry.updateAll(productList);
        })
   		.then(function() {
         	return getAssetRegistry('org.acme.online.shop.Account')
        })
        .then(function(accountListRegistry) {
            return accountListRegistry.updateAll(accountList);
        });
}

/**
 * Set a Product List state
 * @param {org.acme.online.shop.SetProductListState} state - the state
 * @transaction
 */

function setProductListState(productListState) {
  var products = productListState.productList;
  for (i in products) {
    products[i].state = productListState.state;
  }
  return getAssetRegistry('org.acme.online.shop.Product')
  	.then(function(productListRegistry) {
    	return productListRegistry.updateAll(products);
  	});
}

/**
 * Set a Product state RESERVED
 * @param {org.acme.online.shop.SetProductListReserved} ProductListReserved - product list to reserve
 * @transaction
 */

function setProductListReserved(ProductListReserved) {
  var reserveList = ProductListReserved.reserveList;
  var productList = ProductListReserved.reserveList.products;
  
  for (i in productList) {
    var product = productList[i];
    if (product.state != 'FOR_SALE') {
      throw new Error('Product is not FOR SALE!');
    //} else if (ProductListReserved.reserver == product.owner) {
    //  throw new Error('An attempt to reserve your own product FOR SALE!');
    } else {
       product.state = 'RESERVED';
    }
  }
  
  return getAssetRegistry('org.acme.online.shop.ReserveList')
        .then(function(productListRegistry) {
    		return productListRegistry.update(reserveList);
        })
  		.then(function() {
         	return getAssetRegistry('org.acme.online.shop.Product')
        })
        .then(function(productListRegistry) {
            return productListRegistry.updateAll(productList);
        });
}



